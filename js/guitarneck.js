
// GLOBAL VARIABLES/CONSTANTS
const svg_w = 1100;
const svg_h = 1100;

const neck_l = 900;
const neck_w = 300;
const neck_x0 = 80;
const neck_y0 = 10;

const all_intervals = [ "1", "p2", "S2", "p3", "S3", "4", '-5', "5", "p6", "S6", "p7", "S7" ];

const notes = [ 'C', 'C#/Db', 'D', 'D#/Eb', 'E', 'F', 'F#/Gb', 'G', 'G#/Ab', 'A', 'A#/Bb', 'B' ];
var notes_visible = 1; // 1: TRUE, 0: FALSE

var a_minor_pentatonic_visible = 0; // 1: TRUE, 0: FALSE

const no_frets = 12;
const fret_l = neck_l/no_frets;

  const dot_r = 25;
  const font_size = 12;
  //const fret_l = my_l/(no_frets + 1.0);
  const string_sep = neck_w/6;
  const bubble_d = 0.80*string_sep;

  // COLOURS
  // https://www.w3schools.com/tags/ref_colornames.asp
const fretboard_colour = 'BurlyWood';
const fret_colour      = 'DimGray';
const string_colour    = 'DarkGoldenRod';
const dot_colour       = 'Black'
const bubble_opacity = 0.75


function romanize(num) {
  var lookup = {M:1000,CM:900,D:500,CD:400,C:100,XC:90,L:50,XL:40,X:10,IX:9,V:5,IV:4,I:1},roman = '',i;
  for ( i in lookup ) {
    while ( num >= lookup[i] ) {
      roman += i;
      num -= lookup[i];
    }
  }
  return roman;
}


function  place_intervals( svg_canvas, key ){
  switch( key ){
    case 'C':
      fret_number = 3;
      break;
    case 'C#':
      fret_number = 4;
      break;
    default:
  }
  //key_index = notes.findIndex( 'C' );
  //fret_number = 2 + key_index;
  string_number = 2
  center_x = neck_x0 + (fret_number-0.5) * fret_l;
  center_y = neck_y0 + (6-string_number+0.5) * string_sep; 
  var bubble = svg_canvas.circle( bubble_d );
  bubble.addClass('interval')
  bubble.fill("white");
  bubble.opacity( bubble_opacity );
  bubble.center(center_x, center_y);
}


function draw_guitarneck() {
  var svg_canvas = SVG().addTo('body').size(svg_w, svg_h)
  svg_canvas.addClass('svgTop')

  // NECK
  var neck = svg_canvas.rect(neck_l, neck_w);
  neck.fill(fretboard_colour);
  neck.move( neck_x0, neck_y0 );

  var frets = svg_canvas.group()

  // FRETS
  for ( var x=0; x<=no_frets; x++){
    var fret = svg_canvas.line( neck_x0+x*fret_l, neck_y0, neck_x0+x*fret_l, neck_y0+neck_w );
    frets.add( fret )
    fret.stroke({ width:4 });
    // NUT IS BIGGER
    if (x == 0) {
      fret.stroke({ width:8 });
    }
  }

  frets.stroke(fret_colour);

  var fret_numbers = svg_canvas.group();

  // FRET NUMBERS
  for ( var x=1; x<=no_frets; x++){
      var fret_number = svg_canvas.text( romanize(x) );
      fret_numbers.add( fret_number );
      fret_number.center( neck_x0 + (x-1)*fret_l + 0.5*fret_l, neck_y0 + 6.5*string_sep );
  }

  fret_numbers.font( 'size', font_size);
  fret_numbers.addClass('text')

  var strings = svg_canvas.group();

  // STRINGS
  for (y=0; y<6; y++){
    var y_value = neck_y0 + 0.5*string_sep + y*string_sep;
    var string = svg_canvas.line( neck_x0, y_value, neck_x0+neck_l, y_value );
    strings.add( string );
    string.stroke({ width:(0.6*y+2) });
  }

  strings.stroke(string_colour);

  // DECORATION DOTS
  const dot_x = [3,5,7,9,15,17,19,21];
  for (x of dot_x) {
    if ( x <= no_frets ) {
      var dot = svg_canvas.circle(dot_r);
      dot.fill( dot_colour )
      dot.center( neck_x0 + x*fret_l - 0.5*fret_l, neck_y0 + 0.5*neck_w );
    }
  }
  // DOUBLE DOTS
  if ( 12 <= no_frets ) {
    var dot1 = svg_canvas.circle(dot_r);
    var dot2 = svg_canvas.circle(dot_r);
    dot1.fill( dot_colour )
    dot2.fill( dot_colour )
    dot1.center( neck_x0 + 11.5*fret_l , neck_y0 + 1.0*string_sep );
    dot2.center( neck_x0 + 11.5*fret_l , neck_y0 + 5.0*string_sep );
  }
  if ( 24 <= no_frets ) {
    var dot1 = svg_canvas.circle(dot_r);
    var dot2 = svg_canvas.circle(dot_r);
    dot1.fill( dot_colour );
    dot2.fill( dot_colour );
    dot1.center( neck_x0 + 23.5*fret_l , neck_y0 + 1.0*string_sep );
    dot2.center( neck_x0 + 23.5*fret_l , neck_y0 + 5.0*string_sep );
  }

  // NOTES
  // positions of open strings in notes array
  const string_notes = [ 4, 11, 7, 2, 9, 4 ];

  // loop through strings
  for ( i=0; i < string_notes.length; i++ ) {
    string_note = string_notes[i];
    center_y = neck_y0 + 0.5 * string_sep + i * string_sep;

    // loop through frets
    for ( fret=0; fret<=no_frets; fret++ ){
      note_number = string_note + fret;
      if ( note_number >= 12 ) {
        note_number = note_number - 12;
      }
      if ( note_number >= 12 ) {
        note_number = note_number - 12;
      }
      
      center_x = neck_x0 + fret*neck_l/no_frets - 0.5 * neck_l/no_frets;
      
      // CREATE A BUBBLE
      var bubble = svg_canvas.circle( bubble_d );
      bubble.center(center_x, center_y);
      bubble.fill("white");
      bubble.opacity( 0.0 );

      // CREATE A NOTE TEXT
      var note_text = svg_canvas.text( notes[note_number] );
      note_text.addClass('notes');
      note_text.center( center_x,  center_y );

      // ADD CLASS TO BUBBLE
      switch( note_number ){
        case 0:
          bubble.addClass('bubble-c');
          break;
        case 1:
          bubble.addClass('bubble-cis');
          break;
        case 2:
          bubble.addClass('bubble-d');
          break;
        case 3:
          bubble.addClass('bubble-dis');
          break;
        case 4:
          bubble.addClass('bubble-e');
          break;
        case 5:
          bubble.addClass('bubble-f');
          break;
        case 6:
          bubble.addClass('bubble-fis');
          break;
        case 7:
          bubble.addClass('bubble-g');
          break;
        case 8:
          bubble.addClass('bubble-gis');
          break;
        case 9:
          bubble.addClass('bubble-a');
          break;
        case 10:
          bubble.addClass('bubble-ais');
          break;
        case 11:
          bubble.addClass('bubble-b');
          break;
        default:
      }

    }
  }

} // END function draw_neck()



function set_opacity_of_bubbles( array_of_notes, bubble_opacity ){
  for ( i=0; i < array_of_notes.length; i++ ) {
    let bubble_class = "bubble-".concat( array_of_notes[i] );
    var elementList = document.getElementsByClassName( bubble_class );
    for( var thisElement of elementList ) {
      thisElement.style.opacity = bubble_opacity;
    }
  }
}


function hide_all_bubbles() {
  set_opacity_of_bubbles( ['c', 'cis', 'd', 'dis', 'e', 'f', 'fis', 'g', 'gis', 'a', 'ais', 'b'], 0 );
}


function toggle_note_visibility() {
  var elementList = document.getElementsByClassName( 'notes' );
  if ( Boolean(notes_visible) ) {
    for( var thisElement of elementList ) {
      thisElement.style.opacity = 0.0;
    }
    notes_visible = 0;
  } else {
    for( var thisElement of elementList ) {
      thisElement.style.opacity = 1.0;
    }
    notes_visible = 1;
  }
}


function toggle_a_minor_pentatonic() {
  hide_all_bubbles();
  if ( Boolean(a_minor_pentatonic_visible) ) {
    a_minor_pentatonic_visible = 0;
  } else {
    set_opacity_of_bubbles( ['a', 'd', 'g', 'c', 'e'], 1 )
    a_minor_pentatonic_visible = 1;
  }
}

